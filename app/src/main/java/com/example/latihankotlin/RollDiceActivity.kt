package com.example.latihankotlin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_roll_dice)

        val btnRoll: Button = findViewById(R.id.button)
        btnRoll.setOnClickListener{rolldice()}
    }

    private fun rolldice()
    {
        val dice = Dice(6)
        val diceRoll = dice.roll()

        val resultTextView: TextView = findViewById(R.id.textView4)
        resultTextView.text = diceRoll.toString()

        val toast = Toast.makeText(this, "Dice Rolled", Toast.LENGTH_SHORT).show()
    }
}

class Dice(private val numSides: Int)
{
    fun roll():Int{
        return (1..6).random()
    }
}