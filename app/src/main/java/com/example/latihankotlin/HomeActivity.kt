package com.example.latihankotlin

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View

class HomeActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
    }

    fun activityRoll(view: View) {
        startActivity(Intent(this, MainActivity::class.java))
    }

    fun activityProfile(view: View) {
        startActivity(Intent(this, ProfileActivity::class.java))
    }

    fun activityListView(view: View) {
        startActivity(Intent(this, ListviewActivity::class.java))
    }

    fun activityRecyclerview(view: View) {
        startActivity(Intent(this, RecyclerViewActivity::class.java))
    }

    fun activityLatihanFragment(view: View) {
        startActivity(Intent(this, FragmentActivity::class.java))
    }

    fun activityConstraint(view: View) {
        startActivity(Intent(this, ConstraintsTutorialActivity::class.java))
    }

}