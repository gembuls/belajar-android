package com.example.latihankotlin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.TextView

class ProfileActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
    }

    private fun buttonHandlerClick(clickedView: View)
    {
        addNickName(clickedView)
    }

    private fun addNickName(view: View)
    {
        val teks = findViewById<EditText>(R.id.profile_itxt_name)
        val nicknameTextView = findViewById<TextView>(R.id.profile_itxt_name)

        nicknameTextView.text = teks.text
        teks.visibility = View.GONE
    }

}