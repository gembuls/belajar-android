package com.example.latihankotlin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class RecyclerViewActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recycler_view)

        val recyclerview = findViewById<RecyclerView>(R.id.recyclerview)

        recyclerview.layoutManager = LinearLayoutManager(this)

        val data = ArrayList<ItemViewModel>()

        for(i in 1..20){
            data.add(ItemViewModel(R.drawable.nurrahman_image,"Data ke " + i))
        }

        val adapter = CustomAdapterRecyclerView(data)
        recyclerview.adapter = adapter
    }
}